This is repository for jar file with Artem Hurbych bachalor thesis:"User interface for pick path optimization for a Warehouse Management System".

The source code is here: https://gitlab.fel.cvut.cz/hurbyart/user-interface-for-pick-path-optimization-for-a-warehouse-management-system

Run using command:

**java -cp WMS.jar hurbyart.simplifiedWMS.App**